﻿using System;

namespace Prototype
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            //Создаем круг
            Circle c1 = new Circle(1,2,"red",10);
            
            //И его прототипы
            Circle c2 = (Circle)c1.Copy();
            Circle c3 = (Circle) c1.Clone();
            Circle c4 = (Circle) c1.MyClone();
            //Создаем прямоугольник
            Rectangle r1 = new Rectangle(10,10,"black", 15, 30);
            //и его прототипы
            Rectangle r2 = (Rectangle) r1.Copy();
            Rectangle r3 = (Rectangle) r1.Clone();
            Rectangle r4 = (Rectangle) r1.MyClone();
            
            //Выводим данные на экран
            Console.WriteLine("Shape Circle before setting");
            DisplayShape(c1);
            DisplayShape(c2);
            DisplayShape(c3);
            DisplayShape(c4);
            DisplayShape(r1);
            DisplayShape(r2);
            DisplayShape(r3);
            DisplayShape(r4);
            //Переменные после изменения
            Console.WriteLine();
            Console.WriteLine("Выводи данных после присвоения значений переменным классов");
            c1.radius = 5;
            c1.x = 0;
            
            c1.color = "green";
            c2.radius = 8;
            c2.ID.ID = 2;
            c2.color = "brown";
            c3.color = "blue";
            c3.y = 0;
            c3.ID.ID = 3;

            c4.color = "magneta";
            c4.ID.ID = 4;
            
            r1.color = "red";
            r2.color = "cyan";
            r2.ID.ID++;
                
            r3.color = "gray";
            r3.ID.ID = 12;
            r2.width = 25;
            r2.heigth = 50;

            r3.x = 1000;
            r4.color = "yellow";
            r4.heigth = 500;
            r3.ID.ID = 14;
            //Выводим данные на экран
            DisplayShape(c1);
            DisplayShape(c2);
            DisplayShape(c3);
            DisplayShape(c4);
            DisplayShape(r1);
            DisplayShape(r2);
            DisplayShape(r3);
            DisplayShape(r4);
        }

        public static void DisplayShape(Shape shape)
        {
            if (shape.ID !=null)
                Console.Write($"ID={shape.ID.ID} ");
            Console.Write($"{shape.GetType()} x={shape.x} y={shape.y} color={shape.color}");
            
            if (shape is Circle)
            {
                Circle cp = (Circle) shape;
                Console.WriteLine($" radius={cp.radius}");
            }
            else if (shape is Rectangle)
            {
                Rectangle rt = (Rectangle) shape;
                Console.WriteLine($" w={rt.width} h={rt.heigth}");
            }
            
        }

        
    }
}