namespace Prototype
{
    public class IdNum
    {
        public int ID { get; set; }

        public IdNum(int id)
        {
            ID = id;
        }
    }
}