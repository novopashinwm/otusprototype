using System;

namespace Prototype
{
    public class Circle : Shape, ICloneable
    {
        public int radius;
        
        
        public Circle(int x, int y, string color, int radius) 
            : base(x,y,color)
        {
            this.radius = radius;
            ID = new IdNum(1);
        }

        public override Shape Copy()
        {
            Circle clone = (Circle) this.MemberwiseClone();
            return clone;
        }

        public object Clone()
        {
            Object clone = this.MemberwiseClone();
            return clone;
        }
    }
}