using System;

namespace Prototype
{
    public class Rectangle : Shape, ICloneable
    {
 

        public int width;
        public int heigth;
        public Rectangle(int x,int y, string color, 
                int width, int heigth) 
            : base(x,y,color)
        {
            this.width  = width;
            this.heigth = heigth;
            ID = new IdNum(10);
        }

        public override Shape Copy()
        {
            Rectangle clone = (Rectangle) this.MemberwiseClone();
            return clone;
        }

        public object Clone()
        {
            Object clone = this.MemberwiseClone();
            return clone;
        }
    }
}