using System;

namespace Prototype
{
    public interface IMyCloneable
    {
        Object MyClone();
    }
}