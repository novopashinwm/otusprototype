using System;
using System.Management.Instrumentation;

namespace Prototype
{
    public abstract class Shape : IMyCloneable
    {
        public int x;
        public int y;
        public string color;
        public IdNum ID { get;  set; }
        public Shape(int x, int y, string color)
        {
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public abstract Shape Copy();
        public Object MyClone()
        {
            Object clone = this.MemberwiseClone();
            ((Shape)clone).ID = new IdNum(((Shape)clone).ID.ID);
            return clone;
        }
    }
}